﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TriviaServer.Contexts;
using TriviaServer.Models.Game;

namespace TriviaServer.Controllers
{
    [Route("api/")]
    public class InfoController : Controller
    {
        private readonly TriviaContext _triviaContext;

        public InfoController(TriviaContext triviaContext)
        {
            _triviaContext = triviaContext;

            _triviaContext.AddPlayerAsync(new Player());
        }


        [HttpGet]
        [Route("categories")]
        public List<Category> GetCategories()
        {
            var categories = _triviaContext.GetCategories();

            for(var i = 0; i < categories.Count; i++)
            {
                categories[i].Questions = new List<Question>();
            }

            return categories;
        }

        [HttpGet]
        [Route("questions/by_category/{categoryId}")]
        public IActionResult GetQuestionsByCategoryId(string categoryId)
        {
            var category = _triviaContext.GetCategory(categoryId);

            if (category == null)
            {
                return NotFound("Category not found");
            }

            var rand = new Random();
            var question = category.Questions[rand.Next(category.Questions.Count)];

            return Ok(question);
        }

        [HttpGet]
        [Route("players/leaderboard/{daysPeriod}")]
        public IActionResult GetPlayersInDaysPeriod(int daysPeriod)
        {
            if (daysPeriod <= 0)
            {
               return BadRequest("Invalid period");
            }

            var players = _triviaContext.GetPlayersInDaysPeriod(daysPeriod);

            players = players.OrderByDescending(plr => plr.Score).ToList();

            return Ok(players);
        }
    }
}
