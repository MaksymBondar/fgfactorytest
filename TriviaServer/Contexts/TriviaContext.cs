﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using TriviaServer.Models.Game;
using TriviaServer.Models.Settings;

namespace TriviaServer.Contexts
{
    public class TriviaContext
    {
        private Random _random = new Random();

        private IMongoCollection<Player> Players
        {
            get { return _database.GetCollection<Player>("Players"); }
        }

        private IMongoCollection<Category> Categories
        {
            get { return _database.GetCollection<Category>("Categories"); }
        }

        private IMongoCollection<GameplayRoom> GameplayRooms
        {
            get { return _database.GetCollection<GameplayRoom>("GameplayRoom"); }
        }

        private IMongoDatabase _database;
        private IGridFSBucket _gridFS;

        public TriviaContext(IOptions<ConnectionInfo> connectionInfo)
        {
            string connectionString = connectionInfo.Value.TriviaDatabase;
            var connection = new MongoUrlBuilder(connectionString);
            var client = new MongoClient(connectionString);
            _database = client.GetDatabase(connection.DatabaseName);
            _gridFS = new GridFSBucket(_database);
        }


        public async void AddPlayerAsync(Player player)
        {
            await Players.InsertOneAsync(player);
        }

        public List<Player> GetPlayers()
        {
            var filter = new BsonDocument();
            return Players.Find(filter).ToList();
        }

        public List<Player> GetPlayersInDaysPeriod(int daysPeriod)
        {
            var from = DateTime.UtcNow.AddDays(-daysPeriod);
            var to = DateTime.UtcNow;

            var filter = Builders<Player>.Filter.And(Builders<Player>.Filter.Gt("LastGameDate", from), Builders<Player>.Filter.Lt("LastGameDate", to));
            var players = Players.Find(filter);

            return players.ToList();
        }

        public Player GetPlayerByConnectionId(string connectionId)
        {
            var filter = Builders<Player>.Filter.Eq("ConnectionId", connectionId);
            var player = Players.Find(filter).FirstOrDefault();

            return player;
        }

        public Player GetPlayerById(string playerId)
        {
            var filter = Builders<Player>.Filter.Eq("_id", new ObjectId(playerId));
            var player = Players.Find(filter).FirstOrDefault();

            return player;
        }

        public Player CreatePlayer(string connectionId,string characterColor)
        {
            var player = new Player()
            {
                Name = "Player_" + GenerateRandomPlayerName(7),
                Score = 0,
                CharacterColor=characterColor,
                ConnectionId=connectionId
            };
            Players.InsertOne(player);

            return player;
        }

        public void SetPlayerColor(string playerId, string characterColor)
        {
            var filter = Builders<Player>.Filter.Eq("_id", new ObjectId(playerId));
            var update = Builders<Player>.Update.Set("CharacterColor", characterColor);

            Players.UpdateOne(filter, update);
        }


        public List<Category> GetCategories()
        {
            var filter = new BsonDocument();
            return Categories.Find(filter).ToList();
        }

        public Category GetCategory(string categoryId)
        {
            var filter = Builders<Category>.Filter.Eq("_id", new ObjectId(categoryId));
            var category = Categories.Find(filter).FirstOrDefault();

            return category;
        }

        public async void AddCategoryAsync(Category category)
        {
            await Categories.InsertOneAsync(category);
        }

        public Category GenerateCategory(string categoryName)
        {
            var category = new Category()
            {

                Name = categoryName,
                Questions = new List<Question>()
            };
            for (var questionIndex = 0; questionIndex < 50; questionIndex++)
            {
                var question = new Question()
                {
                    Id = ObjectId.GenerateNewId().ToString(),
                    Text = $"Question number {questionIndex + 1} for category '{categoryName}'",
                    Answers=new List<Answer>()
                };
                var correctAnswearIndex = _random.Next(4);
                for(var answerIndex = 0; answerIndex < 4; answerIndex++)
                {
                    var answear = new Answer()
                    {
                        Id=ObjectId.GenerateNewId().ToString(),
                        IsCorrect = answerIndex == correctAnswearIndex,
                        Text = answerIndex == correctAnswearIndex ? "Correct answer" : "Wrong answer"
                    };
                    question.Answers.Add(answear);
                }
                category.Questions.Add(question);
            }
            return category;
        }


        public GameplayRoom GetRoomById(string roomId)
        {
            var filter = Builders<GameplayRoom>.Filter.Eq("_id", new ObjectId(roomId));

            var room = GameplayRooms.Find(filter).FirstOrDefault();
            return room;
        }

        public GameplayRoom GetRoomByPlayerId(string playerId)
        {
            var filter = Builders<GameplayRoom>.Filter.Eq("PlayersIds", playerId);
            var room = GameplayRooms.Find(filter).FirstOrDefault();

            return room;
        }

        public List<GameplayRoom> GetAvailableRooms()
        {
            var filter = Builders<GameplayRoom>.Filter.Size("PlayersIds", 1);
            var rooms = GameplayRooms.Find(filter).ToList();
            return rooms;
        }

        public GameplayRoom CreateRoom(string creatorId)
        {
            var room = new GameplayRoom()
            {
                MaxPlayers = 2,
                PlayersIds = new List<string>()
            };

            GameplayRooms.InsertOne(room);

            JoinToRoom(room.Id, creatorId, true);

            return room;
        }

        public async void RemoveRoomAsync(string roomId)
        {
            await GameplayRooms.DeleteOneAsync(new BsonDocument("_id", new ObjectId(roomId)));
        }

        public GameplayRoom JoinToRoom(string roomId, string playerId, bool IsGameOrganizer)
        {
            var roomFilter = Builders<GameplayRoom>.Filter.Eq("_id", new ObjectId(roomId));
            var roomUpdate = Builders<GameplayRoom>.Update.Push("PlayersIds", playerId);

            GameplayRooms.UpdateOne(roomFilter, roomUpdate);

            var playerFilter = Builders<Player>.Filter.Eq("_id", new ObjectId(playerId));
            var playerUpdate = Builders<Player>.Update.Set("IsGameOrganizer", IsGameOrganizer);

            Players.UpdateOne(playerFilter, playerUpdate);

            return GetRoomById(roomId);
        }


        public void TryFillDatabase()
        {
            if (GetPlayers().Count == 0)
            {
                for (var playerIndex = 0; playerIndex < 100; playerIndex++)
                {
                    var score = _random.Next(700);
                    var gameWasDaysAgo = _random.Next(-60, 0);
                    var player = new Player()
                    {
                        Name ="Player_"+ GenerateRandomPlayerName(7),
                        Score = score,
                        LastGameDate = DateTime.UtcNow.AddDays(gameWasDaysAgo)
                    };

                    AddPlayerAsync(player);
                }
            }
            if (GetCategories().Count == 0)
            {
                AddCategoryAsync(GenerateCategory("Cinema"));
                AddCategoryAsync(GenerateCategory("Biology"));
                AddCategoryAsync(GenerateCategory("Computer"));
                AddCategoryAsync(GenerateCategory("Geography"));
                AddCategoryAsync(GenerateCategory("Weapon"));
                AddCategoryAsync(GenerateCategory("Flags"));
                AddCategoryAsync(GenerateCategory("Physics"));
                AddCategoryAsync(GenerateCategory("Space"));
                AddCategoryAsync(GenerateCategory("Maths"));
                AddCategoryAsync(GenerateCategory("Sport"));
            }
        }
 
        private string GenerateRandomPlayerName(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

            var tempKey = String.Empty;
            for (var charIndex = 0; charIndex < length; charIndex++)
            {
                var isLowerKeys = _random.Next(0, 2) == 0;
                string tempChar = chars[_random.Next(0, chars.Length)].ToString();

                if (isLowerKeys)
                    tempChar = tempChar.ToLower();
                tempKey += tempChar;
            }

            return tempKey;
        }
    }
}

