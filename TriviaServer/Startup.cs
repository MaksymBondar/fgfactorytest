using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TriviaServer.Contexts;
using TriviaServer.Hubs;
using TriviaServer.Models.Settings;

namespace TriviaServer
{
    public class Startup
    {
       private readonly string _triviaAllowSpecificOrigins = "_triviaAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<TriviaContext>();

            var connectionInfoSection = this.Configuration.GetSection("ConnectionInfo");
            services.Configure<ConnectionInfo>(options => connectionInfoSection.Bind(options));

            var allowOrigins = this.Configuration.GetSection("AllowOrigins").Value;

            services.AddControllers();

            services.AddSignalR();

            services.AddCors(options =>
            {
                options.AddPolicy(name: _triviaAllowSpecificOrigins,
                                  builder =>
                                  {
                                      builder.WithOrigins(allowOrigins);
                                      builder.AllowCredentials();
                                      builder.AllowAnyHeader();
                                  });
            });

            var serviceProvider = services.BuildServiceProvider();

            var dbContext = serviceProvider.GetService<TriviaContext>();
            dbContext.TryFillDatabase();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseCors(_triviaAllowSpecificOrigins);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<TriviaHub>("/trivia");
            });
        }
    }
}
