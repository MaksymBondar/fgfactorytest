﻿using System;
namespace TriviaServer.Models.Settings
{
    public class ConnectionInfo
    {
        public string TriviaDatabase { get; set; }
    }
}
