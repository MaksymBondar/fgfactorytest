﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TriviaServer.Models.Game
{
    public class GameplayRoom
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public int MaxPlayers { get; set; }
        public List<string> PlayersIds { get; set; }
    }
}
