﻿using System;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using System.Linq;
using TriviaServer.Contexts;
using TriviaServer.Models.Game;

namespace TriviaServer.Hubs
{
    public class TriviaHub : Hub
    {
        private readonly TriviaContext _triviaContext;

        public TriviaHub(TriviaContext triviaContext)
        {
            _triviaContext = triviaContext;
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            Leave();

            await base.OnDisconnectedAsync(exception);
        }


        public void Join(string characterColor)
        {
            var player = _triviaContext.GetPlayerByConnectionId(Context.ConnectionId);
            if (player == null)
            {
                player = _triviaContext.CreatePlayer(Context.ConnectionId, characterColor);
            }
            else
            {
                _triviaContext.SetPlayerColor(player.Id, characterColor);
            }

            var availableRooms = _triviaContext.GetAvailableRooms();

            GameplayRoom room;

            if (availableRooms.Count > 0)
            {
                room = availableRooms.First();
                room= _triviaContext.JoinToRoom(room.Id, player.Id, false);

                var firstPlayer = _triviaContext.GetPlayerById(room.PlayersIds[0]);
                var secondPlayer = _triviaContext.GetPlayerById(room.PlayersIds[1]);

                SendRoomReadyMessages(firstPlayer,secondPlayer);
                SendRoomReadyMessages(secondPlayer, firstPlayer);
            }
            else
            {
                room = _triviaContext.CreateRoom(player.Id);
            }
        }

        public void Send(string jsonData)
        {
            var player = _triviaContext.GetPlayerByConnectionId(Context.ConnectionId);
            var room = _triviaContext.GetRoomByPlayerId(player.Id);

            var oppenentId = room.PlayersIds[0] != player.Id ? room.PlayersIds[0] : room.PlayersIds[1];
            var opponent = _triviaContext.GetPlayerById(oppenentId);

            Clients.Client(opponent.ConnectionId).SendAsync("Send", jsonData);
        }

        public void Leave()
        {
            var player = _triviaContext.GetPlayerByConnectionId(Context.ConnectionId);
            var room = _triviaContext.GetRoomByPlayerId(player.Id);


            if (room != null)
            {
                var oppenentId = room.PlayersIds[0] != player.Id ? room.PlayersIds[0] : room.PlayersIds[1];
                var opponent = _triviaContext.GetPlayerById(oppenentId);

                Clients.Client(opponent.ConnectionId).SendAsync("OpponentLeave");

                _triviaContext.RemoveRoomAsync(room.Id);
            }
        }

        private void SendRoomReadyMessages(Player opponent,Player player)
        {
            Clients.Client(player.ConnectionId).SendAsync("OpponentJoined", opponent.Name, opponent.CharacterColor, opponent.IsGameOrganizer);
            Clients.Client(player.ConnectionId).SendAsync("CanPlay");
        }
    }
}
