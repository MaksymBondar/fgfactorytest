# Launch (local)
1. Open TriviaServer>appsettings.Development.json and insert mongo db url to:
```sh
  "ConnectionInfo": {
    "TriviaDatabase": "mongodb://localhost:27017/triviaDB"
  }
```
2.(optional) Open TriviaServer>appsettings.json and set allowed url to
```sh
    "AllowOrigins": "http://projects.fgfactory.com"
```
3. Launch application with Development profile

![N|Solid](https://i.ibb.co/MBVJMJx/Screenshot-1.png)

4. Open http://projects.fgfactory.com/trivia_multiplayer/ and  set https://localhost:5001 as  API url

# Deploy
1. Open TriviaServer>appsettings.Production.json and insert remote mongo db url to:
```sh
  "ConnectionInfo": {
    "TriviaDatabase": "mongodb://localhost:27017/triviaDB"
  }
```

2. Change profile to Production

![N|Solid](https://i.ibb.co/XFMCmr9/Screenshot-2.png)

3. Publish application to your host
4. Enjoy :)
For tests I deployed application to [http://maksymbondar.somee.com/](http://maksymbondar.somee.com/)

